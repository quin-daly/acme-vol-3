# iPyParallel - Intro to Parallel Programming
from ipyparallel import Client
import numpy as np
import time
import matplotlib.pyplot as plt

# Problem 1
def initialize():
    """
    Write a function that initializes a Client object, creates a Direct
    View with all available engines, and imports scipy.sparse as spar on
    all engines. Return the DirectView.
    """
    # Initialize client object
    client = Client()
    dview = client[:]
    
    # Import package on all engines
    dview.execute('import scipy.sparse as sparse')
    
    return dview

# Problem 2
def variables(dx):
    """
    Write a function variables(dx) that accepts a dictionary of variables. Create
    a Client object and a DirectView and distribute the variables. Pull the variables back and
    make sure they haven't changed. Remember to include blocking.
    """
    # Initialize client object
    client = Client()
    dview = client[:]
    dview.block = True
    
    # Push the given directory
    dview.push(dx)
    
    # Pull variables back to make sure they havent changed
    return dview.pull(list(dx.keys()))
    

# Problem 3
def prob3(n=1000000):
    """
    Write a function that accepts an integer n.
    Instruct each engine to make n draws from the standard normal
    distribution, then hand back the mean, minimum, and maximum draws
    to the client. Return the results in three lists.
    
    Parameters:
        n (int): number of draws to make
        
    Returns:
        means (list of float): the mean draws of each engine
        mins (list of float): the minimum draws of each engine
        maxs (list of float): the maximum draws of each engine.
    """
    # Initialize the client object
    client = Client()
    dview = client[:]
    dview.block = True
    
    # Import required packages
    dview.execute('import numpy as np')
    
    # Apply draws function to find means, mins, and maxs
    draws = lambda n: [np.mean(np.random.normal(size=n)), 
                       np.min(np.random.normal(size=n)),
                       np.max(np.random.normal(size=n))]
    
    block_results = dview.apply_sync(draws, n)
    
    # Extract means, mins, and maxs
    N = 4 # number of machines
    means = [block_results[i][0] for i in range(N)]
    mins = [block_results[i][1] for i in range(N)]
    maxs = [block_results[i][2] for i in range(N)]
    
    return means, mins, maxs
    
    
# Problem 4
def prob4():
    """
    Time the process from the previous problem in parallel and serially for
    n = 1000000, 5000000, 10000000, and 15000000. To time in parallel, use
    your function from problem 3 . To time the process serially, run the drawing
    function in a for loop N times, where N is the number of engines on your machine.
    Plot the execution times against n.
    """
    # Initialize
    n = [1000000, 5000000, 10000000, 15000000]
    parallel_times = []
    serial_times = []
    
    # Define the serial method
    def serial(n):
        for i in range(4):
            draws = np.random.normal(size=n)
            mean = np.mean(draws)
            mins = np.min(draws)
            maxs = np.max(draws)
    
    # Calculate execution times for parallel and serial processes
    for i in range(4):
        start_time = time.time()
        result = prob3(n[i])
        parallel_times.append(time.time() - start_time)
        
        start_time = time.time()
        result = prob3(n[i])
        serial_times.append(time.time() - start_time)
        
    # Plot the results
    plt.plot(n, parallel_times, label='Parallel')
    plt.plot(n, serial_times, label='Serial')
    plt.xlabel('n')
    plt.ylabel('Execution Time')
    plt.legend()
    
    return plt.show()
    
    
# Problem 5
def parallel_trapezoidal_rule(f, a, b, n=200):
    """
    Write a function that accepts a function handle, f, bounds of integration,
    a and b, and a number of points to use, n. Split the interval of
    integration among all available processors and use the trapezoidal
    rule to numerically evaluate the integral over the interval [a,b].

    Parameters:
        f (function handle): the function to evaluate
        a (float): the lower bound of integration
        b (float): the upper bound of integration
        n (int): the number of points to use; defaults to 200
    Returns:
        value (float): the approximate integral calculated by the
            trapezoidal rule
    """
    # Initialize
    client = Client()
    dview = client[:]
    dview.block = True
    interval = np.linspace(a,b,n)
    
    # Send parts of the interval to each of the engines
    dview.scatter('intervals', interval)
    
    # Define the trapezoid method
    def trap(f,i):
        interval = dview.pull('intervals')[i]
        results = []
        for i in range(len(interval)-1):
            results.append(f(interval[i+1])+f(interval[i]))
            
        return ((interval[1]-interval[0])/2) * np.sum(results)
    
    # Apply the method to dview and sum each entry
    evaluation = []
    for i in range(4):
        evaluation.append(trap(f,i))
    
    return np.sum(evaluation)
    
 
