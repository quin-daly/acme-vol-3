"""Volume 3: Web Scraping.
<Quin Daly>
<Math 405>
<01/17/21>
"""
import requests
from os.path import exists
from bs4 import BeautifulSoup
import re
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np

# Problem 1
def prob1():
    """Use the requests library to get the HTML source for the website 
    http://www.example.com.
    Save the source as a file called example.html.
    If the file already exists, do not scrape the website or overwrite the file.
    """
    # Get the response from the website
    response = requests.get('http://www.example.com')
    
    # Write the source code to an html file if it doesn't exist already
    if exists('example.html') != True:
        file = open('example.html', 'w')
        file.write(response.text)
        file.close()
    
# Problem 2
def prob2(code):
    """Return a list of the names of the tags in the given HTML code.
    Parameters:
        code (str): A string of html code
    Returns:
        (list): Names of all tags in the given code"""
    # Make the BeautfiulSoup object with given html code
    small_soup = BeautifulSoup(code, 'html.parser')
    
    # Find all the tags, then get the name of each tag
    tags = small_soup.find_all(True)
    tag_names = [tags[i].name for i in range(len(tags))]
    
    return tag_names


# Problem 3
def prob3(filename="example.html"):
    """Read the specified file and load it into BeautifulSoup. Return the
    text of the first <a> tag and whether or not it has an href
    attribute.
    Parameters:
        filename (str): Filename to open
    Returns:
        (str): text of first <a> tag
        (bool): whether or not the tag has an 'href' attribute
    """
    # Open html file and load it into BeautifulSoup
    with open(filename, 'r', encoding='utf_8') as file:
        code = file.read()
    
    soup = BeautifulSoup(code, 'html.parser')
    
    # Find first <a> tag and whether or not it has an href attribute
    a_tag = soup.a
    has_href = 'href' in a_tag.attrs
    
    return str(a_tag), has_href


# Problem 4
def prob4(filename="san_diego_weather.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the following tags:

    1. The tag containing the date 'Thursday, January 1, 2015'.
    2. The tags which contain the links 'Previous Day' and 'Next Day'.
    3. The tag which contains the number associated with the Actual Max
        Temperature.

    Returns:
        (list) A list of bs4.element.Tag objects (NOT text).
    """
    # Open html file and load it into BeautifulSoup
    with open(filename, 'r', encoding='utf_8') as file:
        code = file.read()
    
    soup = BeautifulSoup(code, 'html.parser')
    
    # Find tag containing date 'Thursday, January 1, 2015'
    date = soup.find(string='Thursday, January 1, 2015').parent
    
    # Find tags containing links 'Previous Day' and 'Next Day'
    previous_day = soup.find(string=re.compile(r"Previous Day")).parent
    next_day = soup.find(string=re.compile(r"Next Day")).parent
    
    # Find tag with number associated with Actual Max Temperature
    big_tag = soup.find(string='Max Temperature').parent.parent.next_sibling.next_sibling
    small_tag = big_tag.span.span
    
    return [date, previous_day, next_day, small_tag]

# Problem 5
def prob5(filename="large_banks_index.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the tags containing the links to bank data from September 30, 2003 to
    December 31, 2014, where the dates are in reverse chronological order.

    Returns:
        (list): A list of bs4.element.Tag objects (NOT text).
    """
    # Open html file and load it into BeautifulSoup
    with open(filename, 'r', encoding='utf_8') as file:
        code = file.read()
    
    soup = BeautifulSoup(code, 'html.parser')
    
    # Get tags
    dates = ['December 31, 2014','September 30, 2014','June 30, 2014','March 31, 2014',
    'December 31, 2013','September 30, 2013','June 30, 2014','March 31, 2013',
    'December 31, 2012','September 30, 2012','June 30, 2012','March 31, 2012',
    'December 31, 2011','September 30, 2011','June 30, 2011','March 31, 2011',
    'December 31, 2010','September 30, 2010','June 30, 2010','March 31, 2010',
    'December 31, 2009','September 30, 2009','June 30, 2009','March 31, 2009',
    'December 31, 2008','September 30, 2008','June 30, 2008','March 31, 2008',
    'December 31, 2007','September 30, 2007','June 30, 2007','March 31, 2007',
    'December 31, 2006','September 30, 2006','June 30, 2006','March 31, 2006',
    'December 31, 2005','September 30, 2005','June 30, 2005','March 31, 2005',
    'December 31, 2004','September 30, 2004','June 30, 2004','March 31, 2004',
    'December 31, 2003','September 30, 2003']
    tags = []
    for date in dates:
        tags.append(soup.find(string=date).parent)

    return tags


# Problem 6
def prob6(filename="large_banks_data.html"):
    """Read the specified file and load it into BeautifulSoup. Create a single
    figure with two subplots:

    1. A sorted bar chart of the seven banks with the most domestic branches.
    2. A sorted bar chart of the seven banks with the most foreign branches.

    In the case of a tie, sort the banks alphabetically by name.
    """
    # Open html file and load it into BeautifulSoup
    with open(filename, 'r', encoding='utf_8') as file:
        code = file.read()
    
    soup = BeautifulSoup(code, 'html.parser')
    
    # Get each row of the table
    table = soup.findChildren('table')
    table = table[0]
    rows = table.find(['td','tr'])
    cells = rows.findChildren('td')
    data = []
    for cell in cells:
        value = cell.string
        data.append(value)
        
    """# test
    print(data)"""
    
    # Put data on each bank into list element
    df = []
    for i in range(1373):
        df.append(data[:13])
        data = data[13:]
        if len(data) < 12:
            break
    """# test
    print(df)"""
    
    # Create a pandas df
    df = pd.DataFrame(np.array(df))
    df = df.drop(0,1)
    header = df.iloc[0]
    df = df[1:]
    df.columns = header
    
    # Get domestic data
    df['Domestic Branches'] = df['Domestic Branches'].str.replace(',','')
    df['Domestic Branches'] = df['Domestic Branches'].str.replace('.','0')
    df['Domestic Branches'] = df['Domestic Branches'].astype(int)
    domestic = df.sort_values('Domestic Branches', ascending=False)
    domestic = domestic.iloc[:7]
    
    # Get foreign data
    df['Foreign  Branches'] = df['Foreign  Branches'].str.replace(',','')
    df['Foreign  Branches'] = df['Foreign  Branches'].str.replace('.','0')
    df['Foreign  Branches'] = df['Foreign  Branches'].astype(int)
    foreign = df.sort_values('Foreign  Branches', ascending=False)
    foreign = foreign.iloc[:7]
    
    # plot each of them
    fig, axs = plt.subplots(2, figsize=(10,8))
    fig.tight_layout(pad=6.0)
    
    # Domestic
    axs[0].barh(domestic['Bank Name / Holding Co Name'], domestic['Domestic Branches'])
    axs[0].title.set_text('Domestic Branches')
    axs[0].set(xlabel='Number of Branches')
    
    # Foreign
    axs[1].barh(foreign['Bank Name / Holding Co Name'], foreign['Foreign  Branches'])
    axs[1].title.set_text('Foreign Branches')
    axs[1].set(xlabel='Number of Branches')
    
    
    return plt.show()
        
    
    
    
