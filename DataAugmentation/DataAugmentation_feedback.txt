11/16/21 16:50

Problem 1 (5 points):
Score += 5

Problem 2 (10 points):
Score += 10

Problem 3 (5 points):
Score += 5

Problem 4 (10 points):
Score += 10

Problem 5 (10 points):
Score += 10

Problem 6 (5 points):
Score += 5

Code Quality (5 points):
Score += 5

Total score: 50/50 = 100.0%

Excellent!


Comments:
	as it turns out, the precision_recall_fscore_support function is bad for getting accurate recall

-------------------------------------------------------------------------------

